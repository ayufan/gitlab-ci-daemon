package redis

import (
	"fmt"
	"strings"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"github.com/jpillora/backoff"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	redisReconnectTimeout = backoff.Backoff{
		//These are the defaults
		Min:    100 * time.Millisecond,
		Max:    60 * time.Second,
		Factor: 2,
		Jitter: true,
	}
	keyWatchers = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "gitlab_workhorse_keywatcher_keywatchers",
			Help: "The number of keys that is being watched by gitlab-workhorse",
		},
	)
	totalMessages = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "gitlab_workhorse_keywather_total_messages",
			Help: "How many messages gitlab-workhorse has received in total on pubsub.",
		},
	)
)

func init() {
	prometheus.MustRegister(
		keyWatchers,
		totalMessages,
	)
}

type WatcherHandler interface {
	OnStart() error
	OnEnd()
	OnValue(key, value string)
}

func dialPubSub(dialer redisDialerFunc) (redis.Conn, error) {
	conn, err := dialer()
	if err != nil {
		return nil, err
	}

	// Make sure Redis is actually connected
	conn.Do("PING")
	if err := conn.Err(); err != nil {
		conn.Close()
		return nil, err
	}

	return conn, nil
}

func watchChannelInner(conn redis.Conn, channel string, handler WatcherHandler) error {
	defer conn.Close()
	psc := redis.PubSubConn{Conn: conn}
	if err := psc.Subscribe(channel); err != nil {
		return err
	}
	defer psc.Unsubscribe(channel)

	startedOnStart := time.Now()
	logrus.Println("keywatcher:", channel, "OnStart...")
	if err := handler.OnStart(); err != nil {
		return fmt.Errorf("OnStart: %q", err)
	}
	logrus.Println("keywatcher:", channel, "OnStart. Done.", time.Since(startedOnStart))

	defer handler.OnEnd()

	for {
		switch v := psc.Receive().(type) {
		case redis.Message:
			totalMessages.Inc()
			dataStr := string(v.Data)
			msg := strings.SplitN(dataStr, "=", 2)
			if len(msg) != 2 {
				logrus.Errorln(fmt.Errorf("keywatcher: invalid notification: %q", dataStr))
				continue
			}
			key, value := msg[0], msg[1]
			handler.OnValue(key, value)
		case error:
			logrus.Errorln(fmt.Errorf("keywatcher: pubsub receive: %v", v))
			// Intermittent error, return nil so that it doesn't wait before reconnect
			return nil
		}
	}
}

func WatchChannel(channel string, watcher WatcherHandler) {
	logrus.Print("keywatcher: starting process loop")
	for {
		conn, err := dialPubSub(workerDialFunc)
		if err != nil {
			logrus.Errorln(fmt.Errorf("keywatcher: %v", err))
			time.Sleep(redisReconnectTimeout.Duration())
			continue
		}
		redisReconnectTimeout.Reset()

		if err = watchChannelInner(conn, channel, watcher); err != nil {
			logrus.Errorln(fmt.Errorf("keywatcher: process loop: %v", err))
		}
	}
}
