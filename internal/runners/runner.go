package runners

import (
	"sync"
	"time"

	"github.com/Sirupsen/logrus"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/builds"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type RunnerBuildStatus int

const (
	RunnerBuildStatusAdded RunnerBuildStatus = iota
	RunnerBuildStatusConsumed
	RunnerBuildStatusAlreadyAdded
	RunnerBuildStatusNotReady
)

type Runner struct {
	client.Runner

	Token string

	projectBuilds   builds.ProjectBuilds
	runningProjects client.RunnerRunning
	ready           bool

	lock     sync.Mutex
	buildsCh chan *builds.Build
}

func (r *Runner) TagsID() tags.ID {
	return r.Token
}

func (r *Runner) TagsSet() tags.Set {
	return r.Runner.TagsSet
}

func (r *Runner) InitBuilds(c *Controller) {
	r.lock.Lock()
	defer r.lock.Unlock()

	started := time.Now()
	defer func() {
		logrus.Debugln("runner: requested all builds:", r.Token, r.projectBuilds.Count(), time.Since(started))
	}()

	c.builds.WhenSubsetOf(r.TagsSet(), func(build *builds.Build) {
		if !r.projectBuilds.Add(build) {
			return
		}

		select {
		case r.buildsCh <- build:
		default:
		}
	})

	logrus.Println("Runner registered:", r.Token, r.projectBuilds.Count())

	r.ready = true
}

func (r *Runner) AddBuild(build *builds.Build) (RunnerBuildStatus, error) {
	if !r.ready {
		return RunnerBuildStatusNotReady, nil
	}

	r.lock.Lock()
	defer r.lock.Unlock()

	if !r.projectBuilds.Add(build) {
		return RunnerBuildStatusAlreadyAdded, nil
	}

	// add a build, if there's not
	select {
	case r.buildsCh <- build:
		return RunnerBuildStatusConsumed, nil

	default:
		return RunnerBuildStatusAdded, nil
	}
}

func (r *Runner) getBuildBalanced(c *Controller) *builds.Build {
	r.lock.Lock()
	defer r.lock.Unlock()

	// TODO: remove when we request running projects
	r.runningProjects, _ = c.client.GetRunnerRunning(r.Token)

	for {
		build := r.projectBuilds.GetBest(r.runningProjects.Running)
		if build == nil {
			return nil
		}

		if !c.builds.IsValid(build) {
			continue
		}

		return build
	}
}

func (r *Runner) GetBuild(c *Controller, timeout time.Duration) *builds.Build {
	build := r.getBuildBalanced(c)
	if build != nil {
		return build
	}

	if timeout > 0 {
		timeout := time.After(timeout)

		// request a new build till timeout
		for {
			select {
			case build := <-r.buildsCh:
				if c.builds.IsValid(build) {
					return build
				}

				r.Invalidate(build)

			case <-timeout:
				return nil
			}
		}
	}

	return nil
}

func (r *Runner) Invalidate(build *builds.Build) {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.projectBuilds.Remove(build)
}
