package runners

import (
	"errors"
	"time"

	"github.com/Sirupsen/logrus"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/builds"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type Controller struct {
	runners *tags.Tree
	client  *client.GitLab
	builds  *builds.Controller
}

func (c *Controller) WhenIncludesAll(tagsSet tags.Set, fn func(*Runner) bool) {
	c.runners.EnumerateWhenIncludesAll(tagsSet, func(item tags.Item) {
		if fn != nil {
			fn(item.(*Runner))
		}
	})
}

func (c *Controller) requestRunner(token string) (runner *Runner, err error) {
	logrus.Infoln("New runner discovered:", token)
	runnerInfo, err := c.client.GetRunnerInfo(token)
	if err != nil {
		return nil, err
	}

	runner = &Runner{
		Runner:        runnerInfo,
		Token:         token,
		projectBuilds: make(builds.ProjectBuilds),
		buildsCh:      make(chan *builds.Build, 1),
	}

	if !c.runners.Add(runner) {
		return nil, errors.New("runner already registered")
	}

	// request all builds for given runner asynchronously
	go runner.InitBuilds(c)
	return runner, nil
}

func (c *Controller) GetBuild(token string, timeout time.Duration) (runner *Runner, build *builds.Build, err error) {
	runner, _ = c.runners.Get(token).(*Runner)

	if runner == nil {
		runner, err = c.requestRunner(token)
		if err != nil {
			return
		}
	}

	build = runner.GetBuild(c, timeout)
	return
}

func (c *Controller) InvalidateRunner(token string) {
	c.runners.RemoveID(token)
}

func NewController(client *client.GitLab, builds *builds.Controller) (*Controller, error) {
	c := &Controller{
		runners: tags.NewTree(),
		client:  client,
		builds:  builds,
	}
	return c, nil
}
