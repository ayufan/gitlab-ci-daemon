package runners

import (
	"strings"

	"github.com/Sirupsen/logrus"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/builds"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/redis"
)

const runnerNotificationChannel = "runner:notifications"
const runnerDetailsKey = "runner:details:"

type notificationsController struct {
	controller *Controller
}

func (n *notificationsController) OnStart() error {
	return nil
}

func (n *notificationsController) OnEnd() {
}

func (n *notificationsController) OnValue(key, value string) {
	if token := strings.TrimPrefix(key, runnerDetailsKey); token != key {
		logrus.Println("Invalidating runner:", token)
		n.controller.InvalidateRunner(token)
	} else {
		logrus.Errorln("Unknown runner notification:", key, "=", value)
	}
}

func (c *Controller) ProcessNotifications() {
	redis.WatchChannel(runnerNotificationChannel, &notificationsController{controller: c})
}

func (c *Controller) ConsumeBuilds(newBuilds chan *builds.Build) {
	for build := range newBuilds {
		c.WhenIncludesAll(build.TagsSet(), func(runner *Runner) bool {
			result, _ := runner.AddBuild(build)

			// since build was consumed immediately do not process it further
			if result == RunnerBuildStatusConsumed {
				return true
			}

			return false
		})
	}
}
