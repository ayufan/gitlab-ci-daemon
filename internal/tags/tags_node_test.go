package tags

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func measureEnumarateStats(fn func()) {
	enumerateNodeItems = 0
	enumerateAll = 0
	enumerateWhenIncludesAll = 0
	enumerateWhenSubsetOf = 0

	fn()

	if testing.Verbose() {
		println("Enum:",
			"nodeItems:", enumerateNodeItems,
			"all:", enumerateAll,
			"whenIncludesAll:", enumerateWhenIncludesAll,
			"whenSubsetOf:", enumerateWhenSubsetOf)
	}
}

func TestEnumerateAllIncludes(t *testing.T) {
	root := NewNode()
	root.Add(&testItem{id: 1}, Tags{"aa", "bb", "cc", "dd", "ee", "ff"})
	root.Add(&testItem{id: 2}, Tags{"aa", "ff"})
	root.Add(&testItem{id: 3}, Tags{"aa", "cc", "ff"})
	if testing.Verbose() {
		root.Print(0)
	}

	assert.Equal(t, 3, root.EnumerateWhenIncludesAll(Tags{"aa", "ff"}, nil))
	assert.Equal(t, 2, root.EnumerateWhenIncludesAll(Tags{"cc", "ff"}, nil))
	assert.Equal(t, 2, root.EnumerateWhenIncludesAll(Tags{"cc"}, nil))
	assert.Equal(t, 3, root.EnumerateWhenIncludesAll(Tags{"aa"}, nil))
	assert.Equal(t, 0, root.EnumerateWhenIncludesAll(Tags{"aa", "gg"}, nil))
}

func TestEnumerateWhenSubsetOf(t *testing.T) {
	root := NewNode()
	root.Add(&testItem{id: 1}, Tags{"aa", "ff"})
	root.Add(&testItem{id: 2}, Tags{"cc", "ff"})
	root.Add(&testItem{id: 3}, Tags{"cc"})
	root.Add(&testItem{id: 4}, Tags{"aa"})
	root.Add(&testItem{id: 5}, Tags{"gg"})
	if testing.Verbose() {
		root.Print(0)
	}

	assert.Equal(t, 4, root.EnumerateWhenSubsetOf(Tags{"aa", "bb", "cc", "dd", "ee", "ff"}, nil))
	assert.Equal(t, 2, root.EnumerateWhenSubsetOf(Tags{"aa", "ff"}, nil))
	assert.Equal(t, 4, root.EnumerateWhenSubsetOf(Tags{"aa", "cc", "ff"}, nil))
	assert.Equal(t, 3, root.EnumerateWhenSubsetOf(Tags{"aa", "cc", "gg"}, nil))
	assert.Equal(t, 0, root.EnumerateWhenSubsetOf(Tags{"hh"}, nil))
}
