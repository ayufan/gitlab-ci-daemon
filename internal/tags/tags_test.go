package tags

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEqualsExactlyTheSame(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "bb", "cc"}
	assert.True(t, t1.Equals(t2))
}

func TestEqualsDifferentLength(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "bb"}
	assert.False(t, t1.Equals(t2))
}

func TestEqualsDifferentElements(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "ab", "bb"}
	assert.False(t, t1.Equals(t2))
}

func TestIncludesAllExactlyTheSame(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "bb", "cc"}
	assert.True(t, t1.IncludesAll(t2))
}

func TestIncludesAllSubset(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "cc"}
	assert.True(t, t1.IncludesAll(t2))
}

func TestIncludesEmpty(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{}
	assert.True(t, t1.IncludesAll(t2))
}

func TestIncludesMissingElement(t *testing.T) {
	t1 := Tags{"aa", "bb", "cc"}
	t2 := Tags{"aa", "ab"}
	assert.False(t, t1.IncludesAll(t2))
}
