package tags

import "strings"

type Enumerator func(Item)

var enumerateNodeItems = 0
var enumerateAll = 0
var enumerateWhenIncludesAll = 0
var enumerateWhenSubsetOf = 0

type Node struct {
	nodes tagsMap
	items Items
}

func NewNode() *Node {
	return &Node{
		nodes: make(tagsMap),
		items: make(Items),
	}
}

func (n *Node) Empty() bool {
	return len(n.nodes) == 0 && len(n.items) == 0
}

func (n *Node) Add(item Item, tags Tags) {
	node := n

	for _, tag := range tags {
		newNode := node.nodes[tag]
		if newNode == nil {
			newNode = NewNode()
			node.nodes[tag] = newNode
		}
		node = newNode
	}

	node.items[item.TagsID()] = item
}

func (n *Node) Remove(item Item, tags Tags) {
	if len(tags) == 0 {
		delete(n.items, item.TagsID())
		return
	}

	node := n.nodes[tags[0]]
	if node == nil {
		return
	}

	node.Remove(item, tags[1:])

	if node.Empty() {
		delete(n.nodes, tags[0])
	}
}

func (n *Node) enumerateNodeItems(enumerator Enumerator) int {
	if enumerator != nil {
		for _, item := range n.items {
			enumerator(item)
		}
	}

	enumerateNodeItems++

	return len(n.items)
}

func (n *Node) EnumerateAll(enumerator Enumerator) int {
	count := 0

	for _, node := range n.nodes {
		count += node.EnumerateAll(enumerator)
	}

	count += n.enumerateNodeItems(enumerator)
	enumerateAll++
	return count
}

func (n *Node) EnumerateWhenIncludesAll(tags Tags, enumerator Enumerator) int {
	if len(tags) == 0 {
		return n.EnumerateAll(enumerator)
	}

	enumerateWhenIncludesAll++

	count := 0
	for tag, node := range n.nodes {
		switch strings.Compare(tag, tags[0]) {
		case -1:
			count += node.EnumerateWhenIncludesAll(tags, enumerator)
		case 0:
			count += node.EnumerateWhenIncludesAll(tags[1:], enumerator)
		case 1:
			// no-op
		default:
			panic("should not happen")
		}
	}

	return count
}

func (n *Node) EnumerateWhenSubsetOf(tags Tags, enumerator Enumerator) int {
	enumerateWhenSubsetOf++

	count := 0

	for idx, tag := range tags {
		node := n.nodes[tag]

		if node != nil {
			count += node.EnumerateWhenSubsetOf(tags[idx:], enumerator)
		}
	}

	count += n.enumerateNodeItems(enumerator)
	return count
}

func (n *Node) Print(level int) {
	repeat := strings.Repeat(" ", level)

	for tag, node := range n.nodes {
		println(repeat, "N:", tag)
		node.Print(level + 1)
	}

	for subject := range n.items {
		println(repeat, "I:", subject)
	}
}
