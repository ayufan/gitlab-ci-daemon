package tags

import (
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
)

const dataSet = 10000
const randomTags = 5
const minimumTags = 2
const tagRange = 20

func tagNames(n int) Tags {
	tags := Tags{}
	for i := 0; i < n; i++ {
		tags = append(tags, strconv.Itoa(i))
	}
	return tags.Sort()
}

func initBigSet() *Node {
	generator := rand.New(rand.NewSource(1))
	node := NewNode()
	for i := 0; i < dataSet; i++ {
		tags := Tags{}
		tagCount := generator.Int31n(randomTags) + minimumTags
		for j := tagCount; j > 0; j-- {
			tag := int(generator.Int31n(tagRange))
			tags = append(tags, strconv.Itoa(tag))
		}
		node.Add(&testItem{id: i}, tags.Sort())
	}
	return node
}

func BenchmarkTestingBigTagsIncludesAll(b *testing.B) {
	n := initBigSet()
	includesAll := tagNames(tagRange / 2)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		n.EnumerateWhenIncludesAll(includesAll, nil)
	}
}

func BenchmarkTestingBigTagsSubsetOff(b *testing.B) {
	n := initBigSet()
	subsetOff := tagNames(tagRange)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		r := n.EnumerateWhenSubsetOf(subsetOff, nil)
		require.Equal(b, dataSet, r)
	}
}
