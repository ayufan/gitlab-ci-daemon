package tags

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

const sharedSet = "shared"
const runUntaggedTag = "run_untagged"
const protectedTag = "protected"

var buildMatcher = Set{sharedSet: Tags{runUntaggedTag}.Sort()}
var runnerMatcher = Set{sharedSet: Tags{
	runUntaggedTag, "tag_docker", "tag_east-c", "tag_gce", "tag_git-annex",
	"tag_linux", "tag_mongo", "tag_mysql", "tag_postgres", "tag_ruby", "tag_shared"}.Sort()}

type testItem struct {
	id      ID
	tagsSet Set
}

func (t *testItem) TagsID() ID {
	return t.id
}

func (t *testItem) TagsSet() Set {
	return t.tagsSet
}

func buildTreeFromFile(t require.TestingT, file string) *Tree {
	data, err := ioutil.ReadFile(file)
	require.NoError(t, err)

	var jobs [][]interface{}
	err = json.Unmarshal(data, &jobs)
	require.NoError(t, err)

	root := NewTree()

	// [96325332,   125923,85800,false,false,true,null]
	// 0: id
	// 1: project_id
	// 2: namespace_id
	// 3: protected
	// 4: shared_runners_enabled
	// 5: group_runners_enabled
	// 6: tags

	sharedUntagged := 0
	sharedRunnerMatching := 0

	for _, job := range jobs {
		tags := Tags{}

		if job[3] == true {
			tags = append(tags, protectedTag)
		}

		if job[6] != nil {
			for _, tag := range strings.Split(job[6].(string), ",") {
				tags = append(tags, "tag_"+tag)
			}
		} else {
			tags = append(tags, runUntaggedTag)
		}

		tags.Sort()

		item := testItem{id: ID(job[0].(float64)), tagsSet: make(Set)}

		if job[4] == true {
			item.tagsSet[sharedSet] = tags
		}

		item.tagsSet[fmt.Sprintf("*project_%.0f", job[1])] = tags

		if job[5] == true {
			item.tagsSet[fmt.Sprintf("*namespace_%.0f", job[2])] = tags
		}

		root.Add(&item)

		if job[4] == true {
			if tags.IncludesAll(buildMatcher["shared"]) {
				sharedUntagged++
			}

			if runnerMatcher["shared"].IncludesAll(tags) {
				sharedRunnerMatching++
			}
		}
	}

	if testing.Verbose() {
		root.Print()
		println("Shared Untagged:", sharedUntagged)
		println("Shared Runner Matching:", sharedRunnerMatching)
	}
	return root
}

func BenchmarkMatchingAllSharedAndRunUntagged(b *testing.B) {
	root := buildTreeFromFile(b, "fixtures/jobs-queue.json")

	measureEnumarateStats(func() {
		result := root.EnumerateWhenIncludesAll(buildMatcher, nil)
		require.Equal(b, 92, result)
	})

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		result := root.EnumerateWhenIncludesAll(buildMatcher, nil)
		require.Equal(b, 92, result)
	}
}

func BenchmarkMatchingWhenRunnerPickingBuild(b *testing.B) {
	root := buildTreeFromFile(b, "fixtures/jobs-queue.json")

	measureEnumarateStats(func() {
		result := root.EnumerateWhenSubsetOf(runnerMatcher, nil)
		require.Equal(b, 87, result)
	})

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		result := root.EnumerateWhenSubsetOf(runnerMatcher, nil)
		require.Equal(b, 87, result)
	}
}
