package server

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	redisgo "github.com/garyburd/redigo/redis"
	"github.com/gorilla/mux"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/helpers"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/logging"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/redis"
)

const maxTracePatchSize = 1024
const chunkSize = 128 * 1024
const chunkExpireTime = 7 * 24 * time.Hour

func getChunkIndex(bytes int64) (int64, int64) {
	return bytes / chunkSize, bytes % chunkSize
}

func getChunkName(r *http.Request, chunkIndex int64) string {
	vars := mux.Vars(r)
	return fmt.Sprintf("gitlab:ci:trace:%s:chunks:%d", vars["build_id"], chunkIndex)
}

func (s *DaemonServer) parseContentRange(w http.ResponseWriter, r *http.Request) (int64, int64, error) {
	contentRange := r.Header.Get("Content-Range")
	if contentRange == "" {
		return 0, 0, errors.New("missing Content-Range")
	}

	if r.ContentLength == 0 {
		return 0, 0, errors.New("empty body")
	}

	if r.ContentLength > maxTracePatchSize {
		return 0, 0, fmt.Errorf("too big patch: %d", r.ContentLength)
	}

	ranges := strings.Split(contentRange, "-")
	if len(ranges) < 1 {
		return 0, 0, fmt.Errorf("invalid Content-Range: %q", contentRange)
	}

	rangeStart, err := strconv.ParseInt(ranges[0], 10, 64)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to parse range: %q", err)
	}
	rangeEnd := rangeStart + r.ContentLength - 1

	chunkStart, chunkStartOffset := getChunkIndex(rangeStart)
	chunkEnd, _ := getChunkIndex(rangeEnd)
	if chunkStart != chunkEnd {
		// we cannot deal with cross-chunks
		return 0, 0, fmt.Errorf("multiple chunks: %d vs %d", chunkStart, chunkEnd)
	}

	return chunkStart, chunkStartOffset, nil
}

func (s *DaemonServer) processChunk(w http.ResponseWriter, r *http.Request, chunkIndex, chunkOffset int64) {
	body, err := helpers.PeekRequestBody(w, r, maxTracePatchSize)
	if err != nil {
		logging.Error(w, http.StatusInternalServerError, "cannot read body: %q", err)
		return
	}

	conn := redis.Get()
	if conn == nil {
		s.client.Proxy(w, r, "failed to get Redis connection")
		return
	}
	defer conn.Close()

	chunkName := getChunkName(r, chunkIndex)

	result, err := redisgo.Bytes(conn.Do("GET", chunkName))
	if err != nil {
		s.client.Proxy(w, r, "failed to get existing chunk: %q: %q", chunkName, err)
		return
	}

	if int64(len(result)) < chunkOffset {
		s.client.Proxy(w, r, "existing (%d) is smaller than requested (%d)", len(result), chunkOffset)
		return
	}

	result = append(result[0:chunkOffset], body...)
	_, err = conn.Do("SET", chunkName, result, "EX", chunkExpireTime.Seconds())
	if err != nil {
		s.client.Proxy(w, r, "failed to get existing chunk: %q: %q", chunkName, err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	logging.SetMessage(w, "accepted chunk %d at %d of %d bytes", chunkIndex, chunkOffset, len(body))
}

func (s *DaemonServer) patchTrace(w http.ResponseWriter, r *http.Request) {
	chunkIndex, chunkOffset, err := s.parseContentRange(w, r)
	if err != nil {
		s.client.Proxy(w, r, "error: %q", err)
		return
	}

	s.processChunk(w, r, chunkIndex, chunkOffset)
}
