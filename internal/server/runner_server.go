package server

import (
	"net/http"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/builds"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/runners"

	"github.com/gorilla/mux"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/logging"
)

type DaemonServer struct {
	client  *client.GitLab
	runners *runners.Controller
	builds  *builds.Controller
	mux     http.Handler
}

func (s *DaemonServer) init() error {
	r := mux.NewRouter()
	r.HandleFunc("/api/v4/runners", s.proxyRequest).Methods("POST")
	r.HandleFunc("/api/v4/runners", s.proxyRequest).Methods("DELETE")
	r.HandleFunc("/api/v4/runners/verify", s.proxyRequest).Methods("POST")
	r.HandleFunc("/api/v4/jobs/request", s.requestJob).Methods("POST")
	r.HandleFunc("/api/v4/jobs/{build_id}", s.proxyRequest).Methods("PUT")
	r.HandleFunc("/api/v4/jobs/{build_id}/trace", s.patchTrace).Methods("PATCH")
	r.HandleFunc("/api/v4/jobs/{build_id}/artifacts", s.proxyRequest).Methods("POST")
	r.HandleFunc("/api/v4/jobs/{build_id}/artifacts", s.proxyRequest).Methods("GET")
	s.mux = r
	return nil
}

func (s *DaemonServer) proxyRequest(w http.ResponseWriter, r *http.Request) {
	s.client.Proxy(w, r, "proxy by default")
}

func (s *DaemonServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rw := logging.NewResponseWriter(w, r)
	defer rw.Log()
	s.mux.ServeHTTP(rw, r)
}

func NewDaemonServer(client *client.GitLab, runners *runners.Controller, builds *builds.Controller) (*DaemonServer, error) {
	server := &DaemonServer{
		client:  client,
		runners: runners,
		builds:  builds,
	}
	err := server.init()
	if err != nil {
		return nil, err
	}

	return server, nil
}
