package server

import (
	"net/http"
	"time"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/helpers"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/logging"
)

type runnerRequest struct {
	Token string `json:"token,omitempty"`
}

const maxRegisterBodySize = 128 * 1024
const requestBuildTimeout = 30 * time.Second

func readRunnerToken(w http.ResponseWriter, r *http.Request) (string, error) {
	token := r.FormValue("token")
	if token != "" {
		return token, nil
	}

	var runnerRequest runnerRequest
	err := helpers.ReadJson(w, r, &runnerRequest)
	if err != nil {
		return "", err
	}

	return runnerRequest.Token, nil
}

func (s *DaemonServer) requestJob(w http.ResponseWriter, r *http.Request) {
	token, err := readRunnerToken(w, r)
	if err != nil {
		logging.Error(w, http.StatusInternalServerError, "cannot read token: %q", err)
		return
	}

	runner, build, err := s.runners.GetBuild(token, requestBuildTimeout)
	if err != nil {
		logging.Error(w, http.StatusInternalServerError, "cannot get build: %q", err)
		return
	}

	// no new builds
	if build == nil {
		logging.SetMessage(w, "no new builds")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	response, err := s.client.AssignBuild(build.Id, token)
	if err != nil {
		logging.Error(w, http.StatusInternalServerError, "cannot assign build: %q", err)
		return
	}

	s.builds.Invalidate(build)
	runner.Invalidate(build)

	// it means that this build was consumed by someone else
	if response == nil {
		logging.SetMessage(w, "conflicting build: %d", build.Id)
		w.WriteHeader(http.StatusConflict)
		return
	}

	logging.SetMessage(w, "assigned build: %d to %q", build.Id, token)
	helpers.WriteJson(w, http.StatusCreated, response)
}
