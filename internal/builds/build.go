package builds

import (
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type Build struct {
	client.Build
}

func (b *Build) TagsID() tags.ID {
	return b.Id
}

func (b *Build) TagsSet() tags.Set {
	return b.Build.TagsSet
}
