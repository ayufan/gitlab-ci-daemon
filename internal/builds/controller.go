package builds

import (
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type Controller struct {
	client *client.GitLab
	builds *tags.Tree
}

func (c *Controller) AddBuild(build *client.Build) *Build {
	newBuild := &Build{
		Build: *build,
	}

	if c.builds.Add(newBuild) {
		return newBuild
	}
	return nil
}

func (c *Controller) IsValid(build *Build) bool {
	return c.builds.Contains(build)
}

func (c *Controller) Invalidate(build *Build) {
	c.builds.Remove(build)
}

func (c *Controller) WhenSubsetOf(tagsSet tags.Set, fn func(*Build)) {
	c.builds.EnumerateWhenSubsetOf(tagsSet, func(item tags.Item) {
		if fn != nil {
			fn(item.(*Build))
		}
	})
}

func (c *Controller) InvalidateAll() {
	c.builds.Truncate()
}

func NewController(client *client.GitLab) (*Controller, error) {
	c := &Controller{
		client: client,
		builds: tags.NewTree(),
	}
	return c, nil
}
