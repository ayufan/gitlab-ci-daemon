package builds

import "math"

type ProjectBuilds map[int64]*BuildList

func (b ProjectBuilds) Add(build *Build) bool {
	buildList := b[build.ProjectId]
	if buildList == nil {
		buildList = new(BuildList)
		b[build.ProjectId] = buildList
	}

	return buildList.Add(build)
}

func (b ProjectBuilds) Remove(build *Build) bool {
	buildList := b[build.ProjectId]
	if buildList == nil {
		return false
	}

	return buildList.Remove(build)
}

func (b ProjectBuilds) Contains(build *Build) bool {
	buildList := b[build.ProjectId]
	if buildList == nil {
		return false
	}

	return buildList.Contains(build)
}

func (b ProjectBuilds) Count() int {
	count := 0
	for _, buildList := range b {
		count += buildList.Len()
	}
	return count
}

func (b ProjectBuilds) GetBest(used map[int64]int64) *Build {
	var bestProjectId int64 = -1
	var bestUsed int64 = math.MaxInt64

	for projectId := range b {
		if used[projectId] < bestUsed {
			bestUsed = used[projectId]
			bestProjectId = projectId

			if bestUsed == 0 {
				break
			}
		}
	}

	if bestProjectId >= 0 {
		return b[bestProjectId].Pop()
	}

	return nil
}

func (b ProjectBuilds) Print() {
	println("Projects:", len(b))
	for projectId, builds := range b {
		println("Project:", projectId)
		builds.Print()
	}
}
