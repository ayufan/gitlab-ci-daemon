package helpers

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func ReadRequestBody(w http.ResponseWriter, r *http.Request, maxBodySize int64) ([]byte, error) {
	limitedBody := http.MaxBytesReader(w, r.Body, maxBodySize)
	defer limitedBody.Close()

	return ioutil.ReadAll(limitedBody)
}

func PeekRequestBody(w http.ResponseWriter, r *http.Request, maxBodySize int64) ([]byte, error) {
	body, err := ReadRequestBody(w, r, maxBodySize)
	if err != nil {
		return nil, err
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	return body, err
}
