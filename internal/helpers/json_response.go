package helpers

import (
	"encoding/json"
	"errors"
	"net/http"
)

const maxBodySize = 128 * 1024

func WriteJson(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(data)
}

func ReadJson(w http.ResponseWriter, r *http.Request, data interface{}) error {
	if !IsApplicationJson(r) {
		return errors.New("invalid content-type received")
	}

	body, err := ReadRequestBody(w, r, maxBodySize)
	if err != nil {
		return err
	}

	return json.Unmarshal(body, data)
}
