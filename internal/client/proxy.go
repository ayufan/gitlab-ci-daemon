package client

import (
	"fmt"
	"net/http"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/logging"
)

func (g *GitLab) Proxy(w http.ResponseWriter, r *http.Request, reason string, args ...interface{}) {
	logging.SetMessage(w, fmt.Sprintf(reason, args...))
	r.Header.Set("User-Agent", r.UserAgent()+" / "+g.userAgent())
	g.proxy.ServeHTTP(w, r)
}
