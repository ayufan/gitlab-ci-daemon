package client

import (
	"net/http/httputil"
	"net/url"
)

type GitLab struct {
	url   *url.URL
	token string
	proxy *httputil.ReverseProxy
}

func (g *GitLab) init() error {
	g.proxy = httputil.NewSingleHostReverseProxy(g.url)
	return nil
}

func (g *GitLab) userAgent() string {
	return "gitlab-ci-daemon"
}

func NewGitLabClient(urlString, token string) (*GitLab, error) {
	urlParsed, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}

	gitlab := &GitLab{url: urlParsed, token: token}
	err = gitlab.init()
	if err != nil {
		return nil, err
	}
	return gitlab, nil
}
