package client

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/Sirupsen/logrus"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type Build struct {
	Id        int64    `json:"id"`
	ProjectId int64    `json:"project_id"`
	TagsSet   tags.Set `json:"tags_set"`
}

func (g *GitLab) AllPendingBuilds(afterId, limit int64) (builds []*Build, err error) {
	started := time.Now()

	values := url.Values{}
	values.Set("after_id", strconv.FormatInt(afterId, 10))
	values.Set("limit", strconv.FormatInt(limit, 10))

	result, statusText, _ := g.doJSON("/api/v4/jobs/pending?"+values.Encode(), "GET", 200, nil, &builds)

	switch result {
	case http.StatusOK:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: pending builds...", "received", afterId, "-", len(builds), time.Since(started), afterId, "-", limit)
		return
	default:
		logrus.WithField("status", statusText).Errorln("api: pending builds...", "failed", time.Since(started))
		return nil, errors.New(statusText)
	}
}

func (g *GitLab) AssignBuild(buildId int64, runnerToken string) (response interface{}, err error) {
	started := time.Now()
	result, statusText, _ := g.doJSON("/api/v4/jobs/request/"+strconv.FormatInt(buildId, 10),
		"POST", http.StatusCreated, &RunnerRequest{Token: runnerToken}, &response)

	switch result {
	case http.StatusCreated:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: assign build...", "received", buildId, time.Since(started))
		return
	case http.StatusNoContent:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: assign build...", "not accepted", buildId, time.Since(started))
		return
	case http.StatusConflict:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: assign build...", "conflict", buildId, time.Since(started))
		return
	default:
		logrus.WithField("status", statusText).Errorln("api: assign build...", "failed", time.Since(started))
		return nil, errors.New(statusText)
	}
}
